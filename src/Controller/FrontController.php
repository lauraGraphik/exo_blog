<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class FrontController extends AbstractController
{

    /**
     * @Route("/", name="app_index")
     */
    public function index(){
        //$user=$this->getUser();

        if($this->isGranted("ROLE_ADMIN")){
            return $this->render("admin/index.html.twig");
        }
        return $this->render("front/index.html.twig");
    }

    /**
     * @Route("/admin", name="admin_index")
     */
    public function adminIndex(){
        return $this->render("admin/index.html.twig");
    }
}