<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\User;
use App\Form\ArticleType;
use App\Entity\Commentaire;
use App\Form\CommentaireType;
use App\Repository\UserRepository;
use App\Repository\CommentaireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\ArticleRepository;
/**
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/", name="article_index", methods={"GET"})
     */
    public function index(ArticleRepository $articleRepository): Response
    {
         $em = $this->getDoctrine()->getManager();
         $userRepository = $em->getRepository(User::class);

         
        // $user=$userRepository->find(9);
        // dump($user);
        //$user = $this->getDoctrine()->getManager()->getRepository(User::class);
       // $articles=$articleRepository->findBy( ["author"=> $user], ["dateParution"=>"DESC"] );
        $articles=$articleRepository->findBy( [], ["dateParution"=>"DESC"] );
    
        
        return $this->render('article/index.html.twig', [
            'articles' => $articles,
        ]);
    }

    

    /**
     * @Route("/{id}", name="article_show", methods={"GET", "POST"})
     */
    public function show(Article $article, Request $request , CommentaireRepository $commentaireRepo ): Response
    {
        
        $datas=[
            "article"=>$article
        ];
        
        $user=$this->getUser();
        dump($commentaireRepo );
        // $comment=$commentaireRepo->findByAuthor($user);
        $comment=$commentaireRepo->findBy( ["author"=>$user, "article"=>$article ] );
        dump($comment);

        $comment2=$commentaireRepo->findCommentByRoleAndArticle( $article );

        if( $this->isGranted("ROLE_USER")){
            $commentaire = new Commentaire();
            $commentaire
                ->setDateParution(new \DateTime())
                ->setAuthor($this->getUser())
                ->setArticle( $article );
            $form = $this->createForm(CommentaireType::class, $commentaire);

            $form->handleRequest($request);
    
            if ($form->isSubmitted() && $form->isValid()) {
                $commentaire
                    ->setDateParution(new \DateTime());
                

            //     $article->setAuthor($this->getUser());
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($commentaire);
                $entityManager->flush();
   
                return $this->redirectToRoute('article_show', ["id"=>$article->getId()]);
            }
            $datas["form"]=$form->createView();
        }
       
        return $this->render('article/show.html.twig', $datas );
    }

   
}
