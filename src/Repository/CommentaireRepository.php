<?php

namespace App\Repository;

use App\Entity\Commentaire;
use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentaire[]    findAll()
 * @method Commentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaire::class);
    }

    public function findCommentByRoleAndArticle2( Article $article ){
        $qb=$this->createQueryBuilder('c')
        ->innerJoin("c.author", "a")
        ->andWhere("a.username = :nom1")
        ->orWhere("a.username = :nom2")
        ->setParameters([
            "nom1"=>"admin",
            "nom2"=>"admin2"
        ]);
        
        
        dump($qb);
        $sql=$qb->getQuery();
        dump($sql);
        $comments=$sql->getResult();
        dump($comments);
        return $comments;
    }

    public function findCommentByRoleAndArticle( Article $article ){
        $qb=$this->createQueryBuilder('c')
        ->innerJoin("c.author", "a")
        ->andWhere("a.roles LIKE :role")
        ->setParameters([
            "role"=>"%ROLE_ADMIN%"
        ]);
        
        
      
        $sql=$qb->getQuery();
     
        $comments=$sql->getResult();
        
        return $comments;
    }
    // /**
    //  * @return Commentaire[] Returns an array of Commentaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commentaire
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
