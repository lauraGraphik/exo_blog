<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label'=>"Votre nom d'utilisateur",
                'required'=>true,
                'attr'=>[
                    "class"=>"mon_champs",
                    "data-truc"=>"bidule"
                ]
            ] );
        if($options["isAdmin"]){
            $builder->add('roles', ChoiceType::class, [
                "choices"=>[
                    "Admin"=>"ROLE_ADMIN",
                    "Inscrit"=>"ROLE_USER"
                ],
                "multiple"=>true,
                "expanded"=>true,
                "label"=>"Droits de l'utilisateur"
            ]);
        }
           
           $builder->add('password', RepeatedType::class, [
                "type"=>PasswordType::class,
                "first_options"=>[
                    "label"=>"Votre mot de passe"
                ],
                "second_options"=>[
                    "label"=>"Répétez votre mot de passe"
                ],
                "invalid_message"=>"Les deux mots de passes ne matchent pas !!",
                "required"=>true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'isAdmin'=>false
        ]);
    }
}
