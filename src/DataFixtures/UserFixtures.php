<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct( UserPasswordEncoderInterface $encoder){
        $this->encoder=$encoder;
    }
    
    public function load(ObjectManager $manager)
    {

        $admin = new User();

        $user = (new User())
            ->setUsername("inscrit")
            ->setRoles(["ROLE_USER"]);
        $user
            ->setPassword( $this->encoder->encodePassword( $user, "mdp"));

        $admin->setUsername("admin")->setRoles(["ROLE_ADMIN"])->setPassword($this->encoder->encodePassword( $admin, "mdp"));
        $manager->persist($user);
        $manager->persist($admin);

        $manager->flush();
    }
}
